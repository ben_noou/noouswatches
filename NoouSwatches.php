<?php
/**
 *
 * @wordpress-plugin
 * Plugin Name:       NoouSwatches
 * Plugin URI:        http://wearenoou.com/noou-swatches
 * Description:       NoouSwatches allows you to add swatches to a swatch panel. This panel is always accessible and allows for unlimited swatches to be added. An added bonus to this is that you only have to click on the swatch to add it to your clipboard so you can use it!
 * Version:           1.0.8
 * Author:            Ben at Noou
 * Author URI:        http://wearenoou.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       NoouSwatches
 * Domain Path:       /languages
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}
require 'plugin_update_check.php';
$MyUpdateChecker = new PluginUpdateChecker_2_0 (
    'https://kernl.us/api/v1/updates/565ca561b731728f79f6a4db/',
    __FILE__,
    'NoouSwatches',
    1
);
function noou_swatches_admin_enqueue() {
   
    wp_enqueue_style( 'swatches_css', plugin_dir_url( __FILE__ ) . 'css/swatches.css' );
    wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/nanoscroller.css' );
    wp_enqueue_script( 'swatches_js', plugin_dir_url( __FILE__ ) . 'js/swatches.js' );
    wp_enqueue_script( 'zclip_js', plugin_dir_url( __FILE__ ) . 'js/jquery.zeroclipboard.min.js');
    wp_enqueue_script( 'nano_js', plugin_dir_url( __FILE__ ) . 'js/jquery.nanoscroller.min.js');
    wp_enqueue_script( 'tooltip', plugin_dir_url( __FILE__ ) . 'js/tooltip.min.js');
    wp_enqueue_script( 'admin_js', plugin_dir_url( __FILE__ ) . 'js/admin.js', array('tooltip'));
}
add_action( 'admin_enqueue_scripts', 'noou_swatches_admin_enqueue' );
function custom_customize_enqueue() {
       wp_enqueue_style( 'swatches_css', plugin_dir_url( __FILE__ ) . 'css/swatches.css' );
    wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css' );
    wp_enqueue_style( 'bootstrap', plugin_dir_url( __FILE__ ) . 'css/nanoscroller.css' );
    wp_enqueue_script( 'swatches_js', plugin_dir_url( __FILE__ ) . 'js/swatches.js' );
    wp_enqueue_script( 'zclip_js', plugin_dir_url( __FILE__ ) . 'js/jquery.zeroclipboard.min.js');
     wp_enqueue_script( 'nano_js', plugin_dir_url( __FILE__ ) . 'js/jquery.nanoscroller.min.js');
    wp_enqueue_script( 'tooltip', plugin_dir_url( __FILE__ ) . 'js/tooltip.min.js');
    wp_enqueue_script( 'admin_js', plugin_dir_url( __FILE__ ) . 'js/admin.js', array('tooltip'));
}
add_action( 'customize_controls_enqueue_scripts', 'custom_customize_enqueue' );
function swatchesP_hide_css() { ?>
	<style type="text/css">
    	#slideOut {display:none!important;}
    </style>
<?php
}
add_action( 'wp_head', 'swatchesP_hide_css');
function noou_swatches($wp_customize) {
	$_oUtil  = new AdminPageFramework_WPUtility;
	$_aData  = get_option( 'APF_AdvancedSections', array() );
	$_sValue = $_oUtil->getElement(
    $_aData,
    array( 'repeatable_tabbed_section_2', 'my_text_field' ), // dimensional path
    'Some default value'
	);
	echo "<div id='slideOut' class='clearfix ";
    echo $_sValue;
    echo "'>
		<style>
		.nano { background: #fff; height: 230px; }
		.nano .nano-pane   { background: #888; }
		.nano .nano-slider { background: #f00; }
		</style>
		<div id='slideClick'>
    	<img src='".plugin_dir_url( __FILE__ ) . "/img/swatch-icon-rgb.png'>
    	</div>
    	<div id='slideOutContainer'>
    		<div id='slideTitle'>NoouSwatches</div>
    		<div class='nano' style='height:230px;'>
    			<div id='slideContent' class='nano-content'>";
    				$my_options = get_option( 'APF_AdvancedSections' );
    				if(!empty($my_options) === true ){
    					foreach($my_options['repeatable_tabbed_section']['my_color'] as $idx => $color){
    					echo "<div class='swatch' data-hex='". $color . "' data-toggle='tooltip' data-placement='left' data-original-title='".$color." copied to clipboard!'>
								<div class='swatchIcon' style='background-color:" . $color . "'></div>
								<div class='swatchHex'>" . $color . "</div>
						</div>";
    				} } else{
    					echo"You have not added any swatches. Please click <a href='admin.php?page=advanced_sections'>here</a> to add one now.";
		}
    echo "</div></div>
    	<div id='slideFooter'>
    	<a href='admin.php?page=advanced_sections'>+</a>
    	</div>
		</div>
		</div>";
}
add_action( 'all_admin_notices', 'noou_swatches' );
add_action( 'customize_register', 'noou_swatches' );

/* start option page */
if ( ! class_exists( 'AdminPageFramework' ) ) {
    include_once( dirname( __FILE__ ) . '/library/admin-page-framework.php' );
}
// Extend the class
class APF_AdvancedSections extends AdminPageFramework {
 
    // Define the setup() method to set how many pages, page titles and icons etc.
    public function setUp() {
       
        // Set the root menu
        $this->setRootMenuPage( 'Noou Swatches', plugin_dir_url( __FILE__ ) . 'img/menu-icon.png' );        // specifies to which parent menu to add.
       
        // Add the sub menus and the pages
        $this->addSubMenuItems(   
            array(
                'title' =>  'Color Swatches',        // the page and menu title
                'page_slug' =>  'advanced_sections'         // the page slug
            ),
            array(
            	'title' => 'Options',
            	'page_slug' => 'noou_swatches_options'
            ),
            array(
            	'title' => 'Submit Bug/Feature Request',
            	'page_slug' => 'noou_swatches_contact'
            )
        );    
        
        // Add form sections
        $this->addSettingSections(
            'advanced_sections',    // target page slug
            array(
                'section_id' => 'repeatable_tabbed_section',
                'section_tab_slug' => 'repeatable_sections',
                'repeatable'    => false,
                'title' => 'Noou Swatches',
                'description' => 'You can add/remove color swatches here. These will show up in the swatches panel to the right of all admin pages.',               
            )
        );
        $this->addSettingSections(
    'noou_swatches_options',    // target page slug
    array(
        'section_id' => 'repeatable_tabbed_section_2',
        'section_tab_slug' => 'repeatable_sections_2',
        'repeatable'    => false,
        'title' => 'Options',
        'description' => 'Here you will find various plugin options. We hope to provide further options over time.',
    )
);  
    
        // Add form fields
        $this->addSettingFields(
            'repeatable_tabbed_section',    // target page slug
                    
            array(
                'field_id' => 'my_color',
                'type' => 'color',
                'title' => 'Color Swatches',
                'repeatable' => true,
                'sortable' => true,
            )
            
        );
        $this->addSettingFields(
        'repeatable_tabbed_section_2',  // target page slug
            array(    // Single text field
                'field_id'      => 'my_text_field',
                'type'          => 'select',
                'title'         => 'Color Scheme',
        'label'         =>  array(
        
        'light' =>  'Light',
        'dark'  =>  'Dark',
    ),
                   
            ),  
            array( // Submit button
                'field_id'      => 'submit_button',
                'type'          => 'submit',
                            'value' => 'Save Settings',
            ),
            array( // export button,
                'field_id'      => 'export_button',
                'type'          => 'export',
'title' => 'Export your swatches and settings',
            ),
            array( // import button
                'field_id'      => 'import_button',
                'type'          => 'import',
'title' => 'Import Settings & Swatches',
'description' => 'Here you can upload a previously exported JSON file. <br>Importing a previously exported file will update the settings and also import your previous swatches.<br><strong> Importing will not overwrite your current swatches however it <span style="color:red;">WILL overwrite your current settings.</span></strong>',
            )
        ); 
        
        
        
       
    }
    //public function footer_right_APF_AdvancedSections( $sContent ) {

         //return $sContent . '<span>Version'.$wp_version.'</span>';
   // }
      public function do_noou_swatches_contact() {   
 
    ?>
   <h1>Submit Bug Report/Feature Request</h1><br>
        <p>We would love to hear from you if you have found a bug in our plugin or if you have a feature request.</p>
        <p>Please complete the form below and we will add this to our list. </p>
        <p>Thank you</p>
        
        <iframe src="//www.wearenoou.com/gfembed/?f=5" width="100%" height="500" frameBorder="0" class="gfiframe"></iframe>
<script src="//www.wearenoou.com/noou/wp-content/plugins/gravity-forms-iframe-master/assets/scripts/gfembed.min.js" type="text/javascript"></script>

    <?php
 
}
/**
 * @callback        filter      import_mime_types_{page slug}
 */
public function import_mime_types_noou_swatches_options( $aMineTypes ) {
    $aMineTypes[] = 'application/json';
    $aMineTypes[] = 'application/txt';
$aMineTypes[] = 'txt/html';
    return $aMineTypes;
}

public function do_advanced_sections() {    // do_ + page slug            
	submit_button();
} 

public function import_noou_swatches_options( $aImportedData, $aStoredData ) {
	$_oUtil  = new AdminPageFramework_WPUtility;
	$_aData  = get_option( 'APF_AdvancedSections', array() );
	$_sValue = $_oUtil->getElement(
    	$_aData,
    	array( 'repeatable_tabbed_section','my_color' ), // dimensional path
    	'Some default value'
		);
    $_aImportedColors = $this->oUtil->getElementAsArray(
        $aImportedData,
        array( 'repeatable_tabbed_section', 'my_color' )
    );
    $aImportedData[ 'repeatable_tabbed_section'][ 'my_color' ] = array_merge( $_sValue, $_aImportedColors );
	return $aImportedData;
	}
}
// Instantiate the class object.
if ( is_admin() ) {
    new APF_AdvancedSections;
}

