=== NoouSwatches ===
Requires at least: 4.0
Tested up to: 4.3.1
License: GPL

A simple color swatch panel plugin.

== Description ==
NoouSwatches is an easy to use color swatch panel. Add your colors and use them within WordPress. It's as simple as that.

== Installation ==
Upload the .zip into WordPress and activate the plugin after the upload. 

Once activated you will be able to add your swatches. 