 jQuery(document).ready(function($) {
     $("body")
         .on("copy", ".swatch", function(e) {
             e.clipboardData.clearData();
             e.clipboardData.setData("text/plain", $(this).data('hex'));
             e.preventDefault();
             var swatch = $(this);
             swatch.tooltip('show');

             swatch.mouseleave(function(event) {
                 $(this).tooltip('destroy');
             });

         });
		 $(".nano").nanoScroller({ alwaysVisible: true });


 });