<?php
class AdminPageFramework_Message {
    public $aMessages = array();
    public $aDefaults = array('option_updated' => 'The options have been updated.', 'option_cleared' => 'The options have been cleared.', 'export' => 'Export', 'export_options' => 'Export Options', 'import_options' => 'Import', 'import_options' => 'Import Options', 'submit' => 'Submit', 'import_error' => 'An error occurred while uploading the import file.', 'uploaded_file_type_not_supported' => 'The uploaded file type is not supported: %1$s', 'could_not_load_importing_data' => 'Could not load the importing data.', 'imported_data' => 'The uploaded file has been imported.', 'not_imported_data' => 'No data could be imported.', 'upload_image' => 'Upload Image', 'use_this_image' => 'Use This Image', 'insert_from_url' => 'Insert from URL', 'reset_options' => 'Are you sure you want to reset the options?', 'confirm_perform_task' => 'Please confirm your action.', 'specified_option_been_deleted' => 'The specified options have been deleted.', 'nonce_verification_failed' => 'A problem occurred while processing the form data. Please try again.', 'check_max_input_vars' => 'Not all form fields could not be sent. Please check your server settings of PHP <code>max_input_vars</code> and consult the server administrator to increase the value. <code>max input vars</code>: %1$s. <code>$_POST</code> count: %2$s', 'send_email' => 'Is it okay to send the email?', 'email_sent' => 'The email has been sent.', 'email_scheduled' => 'The email has been scheduled.', 'email_could_not_send' => 'There was a problem sending the email', 'title' => 'Title', 'author' => 'Author', 'categories' => 'Categories', 'tags' => 'Tags', 'comments' => 'Comments', 'date' => 'Date', 'show_all' => 'Show All', 'show_all_authors' => 'Show all Authors', 'powered_by' => 'Powered by', 'settings' => 'Settings', 'manage' => 'Manage', 'select_image' => 'Select Image', 'upload_file' => 'Upload File', 'use_this_file' => 'Use This File', 'select_file' => 'Select File', 'remove_value' => 'Remove Value', 'select_all' => 'Select All', 'select_none' => 'Select None', 'no_term_found' => 'No term found.', 'select' => 'Select', 'insert' => 'Insert', 'use_this' => 'Use This', 'return_to_library' => 'Return to Library', 'queries_in_seconds' => '%1$s queries in %2$s seconds.', 'out_of_x_memory_used' => '%1$s out of %2$s MB (%3$s) memory used.', 'peak_memory_usage' => 'Peak memory usage %1$s MB.', 'initial_memory_usage' => 'Initial memory usage  %1$s MB.', 'allowed_maximum_number_of_fields' => 'The allowed maximum number of fields is {0}.', 'allowed_minimum_number_of_fields' => 'The allowed minimum number of fields is {0}.', 'add' => 'Add', 'remove' => 'Remove', 'allowed_maximum_number_of_sections' => 'The allowed maximum number of sections is {0}', 'allowed_minimum_number_of_sections' => 'The allowed minimum number of sections is {0}', 'add_section' => 'Add Section', 'remove_section' => 'Remove Section', 'toggle_all' => 'Toggle All', 'toggle_all_collapsible_sections' => 'Toggle all collapsible sections', 'reset' => 'Reset', 'yes' => 'Yes', 'no' => 'No', 'on' => 'On', 'off' => 'Off', 'enabled' => 'Enabled', 'disabled' => 'Disabled', 'supported' => 'Supported', 'not_supported' => 'Not Supported', 'functional' => 'Functional', 'not_functional' => 'Not Functional', 'too_long' => 'Too Long', 'acceptable' => 'Acceptable', 'no_log_found' => 'No log found.',);
    protected $_sTextDomain = 'NoouSwatches';
    static private $_aInstancesByTextDomain = array();
    public static function getInstance($sTextDomain = 'NoouSwatches') {
        $_oInstance = isset(self::$_aInstancesByTextDomain[$sTextDomain]) && (self::$_aInstancesByTextDomain[$sTextDomain] instanceof AdminPageFramework_Message) ? self::$_aInstancesByTextDomain[$sTextDomain] : new AdminPageFramework_Message($sTextDomain);
        self::$_aInstancesByTextDomain[$sTextDomain] = $_oInstance;
        return self::$_aInstancesByTextDomain[$sTextDomain];
    }
    public static function instantiate($sTextDomain = 'NoouSwatches') {
        return self::getInstance($sTextDomain);
    }
    public function __construct($sTextDomain = 'NoouSwatches') {
        $this->_sTextDomain = $sTextDomain;
        $this->aMessages = array_fill_keys(array_keys($this->aDefaults), null);
    }
    public function getTextDomain() {
        return $this->_sTextDomain;
    }
    public function get($sKey) {
        return isset($this->aMessages[$sKey]) ? __($this->aMessages[$sKey], $this->_sTextDomain) : __($this->{$sKey}, $this->_sTextDomain);
    }
    public function output($sKey) {
        echo $this->get($sKey);
    }
    public function __($sKey) {
        return $this->get($sKey);
    }
    public function _e($sKey) {
        $this->output($sKey);
    }
    public function __get($sPropertyName) {
        return isset($this->aDefaults[$sPropertyName]) ? $this->aDefaults[$sPropertyName] : $sPropertyName;
    }
    private function __doDummy() {
        __('The options have been updated.', 'NoouSwatches');
        __('The options have been cleared.', 'NoouSwatches');
        __('Export', 'NoouSwatches');
        __('Export Options', 'NoouSwatches');
        __('Import', 'NoouSwatches');
        __('Import Options', 'NoouSwatches');
        __('Submit', 'NoouSwatches');
        __('An error occurred while uploading the import file.', 'NoouSwatches');
        __('The uploaded file type is not supported: %1$s', 'NoouSwatches');
        __('Could not load the importing data.', 'NoouSwatches');
        __('The uploaded file has been imported.', 'NoouSwatches');
        __('No data could be imported.', 'NoouSwatches');
        __('Upload Image', 'NoouSwatches');
        __('Use This Image', 'NoouSwatches');
        __('Insert from URL', 'NoouSwatches');
        __('Are you sure you want to reset the options?', 'NoouSwatches');
        __('Please confirm your action.', 'NoouSwatches');
        __('The specified options have been deleted.', 'NoouSwatches');
        __('A problem occurred while processing the form data. Please try again.', 'NoouSwatches');
        __('Not all form fields could not be sent. Please check your server settings of PHP <code>max_input_vars</code> and consult the server administrator to increase the value. <code>max input vars</code>: %1$s. <code>$_POST</code> count: %2$s', 'NoouSwatches');
        __('Is it okay to send the email?', 'NoouSwatches');
        __('The email has been sent.', 'NoouSwatches');
        __('The email has been scheduled.', 'NoouSwatches');
        __('There was a problem sending the email', 'NoouSwatches');
        __('Title', 'NoouSwatches');
        __('Author', 'NoouSwatches');
        __('Categories', 'NoouSwatches');
        __('Tags', 'NoouSwatches');
        __('Comments', 'NoouSwatches');
        __('Date', 'NoouSwatches');
        __('Show All', 'NoouSwatches');
        __('Show All Authors', 'NoouSwatches');
        __('Powered by', 'NoouSwatches');
        __('Settings', 'NoouSwatches');
        __('Manage', 'NoouSwatches');
        __('Select Image', 'NoouSwatches');
        __('Upload File', 'NoouSwatches');
        __('Use This File', 'NoouSwatches');
        __('Select File', 'NoouSwatches');
        __('Remove Value', 'NoouSwatches');
        __('Select All', 'NoouSwatches');
        __('Select None', 'NoouSwatches');
        __('No term found.', 'NoouSwatches');
        __('Select', 'NoouSwatches');
        __('Insert', 'NoouSwatches');
        __('Use This', 'NoouSwatches');
        __('Return to Library', 'NoouSwatches');
        __('%1$s queries in %2$s seconds.', 'NoouSwatches');
        __('%1$s out of %2$s MB (%3$s) memory used.', 'NoouSwatches');
        __('Peak memory usage %1$s MB.', 'NoouSwatches');
        __('Initial memory usage  %1$s MB.', 'NoouSwatches');
        __('The allowed maximum number of fields is {0}.', 'NoouSwatches');
        __('The allowed minimum number of fields is {0}.', 'NoouSwatches');
        __('Add', 'NoouSwatches');
        __('Remove', 'NoouSwatches');
        __('The allowed maximum number of sections is {0}', 'NoouSwatches');
        __('The allowed minimum number of sections is {0}', 'NoouSwatches');
        __('Add Section', 'NoouSwatches');
        __('Remove Section', 'NoouSwatches');
        __('Toggle All', 'NoouSwatches');
        __('Toggle all collapsible sections', 'NoouSwatches');
        __('Reset', 'NoouSwatches');
        __('Yes', 'NoouSwatches');
        __('No', 'NoouSwatches');
        __('On', 'NoouSwatches');
        __('Off', 'NoouSwatches');
        __('Enabled', 'NoouSwatches');
        __('Disabled', 'NoouSwatches');
        __('Supported', 'NoouSwatches');
        __('Not Supported', 'NoouSwatches');
        __('Functional', 'NoouSwatches');
        __('Not Functional', 'NoouSwatches');
        __('Too Long', 'NoouSwatches');
        __('Acceptable', 'NoouSwatches');
        __('No log found.', 'NoouSwatches');
    }
}