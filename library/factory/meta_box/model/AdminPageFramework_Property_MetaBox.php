<?php
class AdminPageFramework_Property_MetaBox extends AdminPageFramework_Property_Base {
    public $_sPropertyType = 'post_meta_box';
    public $sMetaBoxID = '';
    public $sTitle = '';
    public $aPostTypes = array();
    public $aPages = array();
    public $sContext = 'normal';
    public $sPriority = 'default';
    public $sClassName = '';
    public $sCapability = 'edit_posts';
    public $sThickBoxTitle = '';
    public $sThickBoxButtonUseThis = '';
    public $aHelpTabText = array();
    public $aHelpTabTextSide = array();
    public $sFieldsType = 'post_meta_box';
    public function __construct($oCaller, $sClassName, $sCapability = 'edit_posts', $sTextDomain = 'NoouSwatches', $sFieldsType = 'post_meta_box') {
        parent::__construct($oCaller, null, $sClassName, $sCapability, $sTextDomain, $sFieldsType);
    }
    protected function _getOptions() {
        return array();
    }
}